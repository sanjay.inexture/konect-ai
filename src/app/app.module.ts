import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppHttpInterceptor } from './core/http-interceptor';
import { APP_BASE_HREF } from '@angular/common';

/** Import all required modules */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { ToastrModule } from 'ngx-toastr';
import { TagCloudModule } from 'angular-tag-cloud-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgApexchartsModule } from 'ng-apexcharts';

/** Import all components */
import { AppComponent } from './app.component';
import { HeaderComponent } from './main/pages/header/header.component';
import { OverviewComponent } from './main/pages/overview/overview.component';
import { HeatmapCalanderComponent } from './main/pages/heatmap-calander/heatmap-calander.component';
import { WordCloudComponent } from './main/pages/word-cloud/word-cloud.component';
import { GraphComponent } from './main/pages/graph/graph.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    OverviewComponent,
    GraphComponent,
    HeatmapCalanderComponent,
    WordCloudComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    TagCloudModule,
    HttpClientModule,
    NgApexchartsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true },
    { provide: APP_BASE_HREF, useValue: "/" },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
