import { GraphComponent } from './main/pages/graph/graph.component';
import { OverviewComponent } from './main/pages/overview/overview.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    component:OverviewComponent
  },
  {
    path: "",
    redirectTo: "/",
    pathMatch: "full"
  },
  {
    path: '**',
    redirectTo: "overview",
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
