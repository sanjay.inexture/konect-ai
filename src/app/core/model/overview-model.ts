export interface IDashboardObj{
    days: number,
    type?: string,
    lead_source: string[]
}

export interface IInBoundList {
    campaign_name : string,
    leads: number,
    positive: number,
    calls: number,
    appointments: number
}

export interface IOutBoundList {
    campaign_name: string,
    leads: number,
    positive: number,
    calls: number,
    appointments: number
}

export interface IDashboardDetails {
    current: number,
    previous: number,
    change: string,
    complete: number
}

export interface IDashboardList extends IDashboardDetails {
    leads: {
        positive: IDashboardDetails,
        negative: IDashboardDetails,
        noresponse: IDashboardDetails,
        optout: IDashboardDetails,
        bad: IDashboardDetails
    },
    messages: {
        sent: IDashboardDetails,
        received: IDashboardDetails
    },
    calls: {
        triggered: IDashboardDetails,
        answered: IDashboardDetails,
        missed: IDashboardDetails
    },
    appointments: IDashboardDetails
}

