import { IDashboardObj } from './../../model/overview-model';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../environments/environment';
import { map } from 'rxjs/operators';
import { apiInfo } from './../../../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';


const hostname = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})
export class OverviewService {
  public heatMapSubject: Subject<any> = new Subject()
  public heatMapSubscription = this.heatMapSubject.asObservable();
  constructor(private http: HttpClient) {}

  /**
   * get dashboard data
   * @param data data
   * @returns response object of api
   */
  public getDashboardData(postObj: IDashboardObj){
    return this.http.post(hostname + apiInfo.info.report, postObj).pipe(
      map((res:any) => res)
    )
  }

  /**
   * get leadSource data
   * @returns response object of api
   */
  public getLeadSourceData(): Observable<any> {
    return this.http.get(hostname + apiInfo.info.leadSource).pipe(
      map((res: any) => res)
    )
  }

  /**
   * get inbound and outbound data
   * @returns response object of api
   */
  public getBoundData(postObj: IDashboardObj): Observable<any> {
    return this.http.post(hostname + apiInfo.info.campaign, postObj).pipe(
      map((res: any) => res)
    )
  }

  /**
   * get heatmap data
   * @returns response object of api
   */
  public getHeapMapData(postObj: IDashboardObj): Observable<any> {
    return this.http.post(hostname + apiInfo.info.heatmap, postObj).pipe(
      map((res: any) => res)
    )
  }

  /**
  * get word cloud data
  * @returns response object of api
  */
  public getWordCloudData(postObj: IDashboardObj): Observable<any> {
    return this.http.post(hostname + apiInfo.info.wordCloud, postObj).pipe(
      map((res: any) => res)
    )
  }
}
