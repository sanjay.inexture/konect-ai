import { interval } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: "root",
})
export class UtilService {
  toast: any;
  authToken: any;
  toastr_classes = {
    error: "toast-error",
    info: "toast-info",
    success: "toast-success",
    warning: "toast-warning",
  };
  public timers = interval(1000);

  constructor(
    private toasterService: ToastrService,
    private router: Router,
  ) {}

  /**
   * set all local storage data with encryption
   */
  setLocalStorageData(dataName: any, dataValue: any) {
    if (dataValue != "" || dataValue != null) {
      localStorage.setItem(dataName, btoa(dataValue));
    }
  }

  /**
   * get all local storage data with decryption
   */
  getLocalStorageData(dataName: any, dataType: any): string | null {
    let dataValue = null;
    if (dataType == "json") {
      dataValue = localStorage.getItem(dataName)
        ? JSON.parse(atob(localStorage.getItem(dataName)!))
        : null;
    } else {
      dataValue = localStorage.getItem(dataName)
        ? atob(localStorage.getItem(dataName)!)
        : null;
    }
    if (dataValue != "" || dataValue != null) {
      dataValue = dataValue;
      return dataValue;
    } else {
      return null;
    }
  }

  /**
   * remove local storage data
   */
  removeLocalStorageData(dataName: any) {
    localStorage.removeItem(dataName);
  }

  /**
   * Clear local storage data when user logout
   */
  clearLocalStorage() {
    localStorage.clear();
  }

  /**
   * Show error message
   * @param title
   * @param message
   */
  showError(title: any, message: any) {
    if (this.toast !== undefined) {
      this.toasterService.clear();
    }
    this.toast = this.toasterService.show(
      message,
      title,
      {},
      this.toastr_classes.error
    );
  }

  /**
   * Show success message
   * @param title
   * @param message
   */
  showSuccess(title: any, message: any) {
    if (this.toast !== undefined) {
      this.toasterService.clear();
    }
    this.toast = this.toasterService.show(
      message,
      title,
      {},
      this.toastr_classes.success
    );
  }

}
