import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, } from '@angular/common/http';
import { UtilService } from './services/util/util.service';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError, timer } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

    timer: any;
    private user: any;
    constructor(private utilService: UtilService, private _toastr: ToastrService) {
        this.timer = timer(1000);
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Clone the request to add the new header.
        const authReq = req.clone({
            setHeaders: {}
        });


        return next.handle(authReq).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // do stuff with response if you want
                }
                return event;
            }),
            catchError((errorResponse: HttpErrorResponse) => {
                if (errorResponse.status === 400) {
                    const errors = errorResponse.error.hasOwnProperty('errors') ? errorResponse.error['errors'] : errorResponse.error;
                    this._toastr.error(errors?.message);
                } else if (errorResponse.status === 401) {
                    this._toastr.error(errorResponse?.error?.message)
                } else if (errorResponse.status === 403) {
                    this._toastr.error('Access Denied')
                } else if (errorResponse.status === 404) {
                    this._toastr.error('Something went wrong');
                } else if (errorResponse.status === 408) {
                    this._toastr.error('Request Timeout');
                } else if (errorResponse.status === 500) {
                    this._toastr.error(errorResponse?.error?.message);
                }
                return throwError(errorResponse.message);
            })
        );
    }
}