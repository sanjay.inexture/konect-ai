import { Component, Input, OnInit, ViewChild } from '@angular/core';
import Chart from 'chart.js/auto';
import * as moment from 'moment';
declare var $: any
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit {
  lineChart: Chart;
  lineChartFromDate: Date = moment().clone().subtract(6, 'days').toDate();
  lineChartToDate: Date = moment().toDate();
  lineChartOptions: any;
  lineChartLables: string[] = []
  lineChartDataset: any = [
    {
      data: [65, 59, 180, 81, 456, 55, 40, 408, 447, 375, 234],
      label: "Changes",
      fill: false,
      borderColor: '#F26957',
      pointColor: "#F26957",
      pointBorderColor: "#F26957",
      pointBackgroundColor: "#F26957",
      pointBorderWidth: 2,
      tension: 0.1,
      borderWidth: 1
    }
    , {
      data: [168, 170, 178, 490, 203, 276, 408, 547, 675, 734],
      label: "Leads",
      borderColor: "#14B79B",
      pointColor: "#14B79B",
      pointBorderColor: "#14B79B",
      pointBackgroundColor: "#14B79B",
      pointBorderWidth: 2,
      fill: false,
      tension: 0.1,
      borderWidth: 1
    }, {
      data: [340, 20, 410, 16, 324, 438, 74, 167, 508, 784],
      label: "Engagement",
      borderColor: "#306DCC",
      pointColor: "#306DCC",
      pointBorderColor: "#306DCC",
      pointBackgroundColor: "#306DCC",
      pointBorderWidth: 2,
      fill: false,
      tension: 0.1,
      borderWidth: 1,
    }, {
      hidden: false,
      data: [0, 150, 130, 122, 560, 26, 500, 172, 312, 433],
      label: "Prev. Leads",
      borderColor: "#BDD7D2",
      pointColor: "#BDD7D2",
      pointBorderColor: "#BDD7D2",
      pointBackgroundColor: "#BDD7D2",
      pointBorderWidth: 2,
      fill: false,
      tension: 0.1,
      borderWidth: 1,
      borderDash: [15]
    }
  ];
  lineChartData: any = {
    labels: this.lineChartLables,
    datasets: this.lineChartDataset
  };

  constructor() { }

  ngOnInit(): void {
    this.getCurrentWeek()
    this.intializeGraph()
  }

  /**
   * get week array
   * @returns full week date in array format
   */
  getCurrentWeek() {
    var dateArray = [];
    let startDate = moment().clone().subtract(6, 'days');
    var currentDate = moment(startDate);
    var stopDate = moment();
    while (currentDate <= stopDate) {
      this.lineChartLables.push(moment(currentDate).format("MMM DD"))
      currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
  }

  /**
 * intialize the lineChart with the data
 */
  intializeGraph() {
    //generate lineChart with data
    this.lineChartData = {
      labels: this.lineChartLables,
      datasets: this.lineChartDataset
    }

    this.lineChartOptions = {
      responsive: true,
      legend: {
        display: false,
      },
      scales: {
        x: {
          beginAtZero: false,
          grid: {
            display: false,
          },
        },
        y: {
          grid: {
            drawBorder: false,
            lineWidth: 0.5,
          }
        }
      },
      plugins: {
        chartAreaBorder: {
          borderColor: 'white'
        },
        legend: {
          display: false
        },
        tooltip: {
          enabled: false,
          external: (context) => {
            // Tooltip Element
            let tooltipEl = document.getElementById('chartjs-tooltip');

            // Create element on first render
            if (!tooltipEl) {
              tooltipEl = document.createElement('div');
              tooltipEl.id = 'chartjs-tooltip';
              tooltipEl.innerHTML = '<table></table>';
              document.body.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            const tooltipModel = context.tooltip;
            if (tooltipModel.opacity === 0) {
              tooltipEl.style.opacity = '0';
              return;
            }

            // Set caret Position (above, below,no-transform ).As I need above I don't delete that class
            tooltipEl.classList.remove('below', 'no-transform');


            // Set HTML & Data
            if (tooltipModel.body) {
              const dataFromCurrentElement = tooltipModel.dataPoints[0];
              const dataSetIndex = dataFromCurrentElement['datasetIndex']
              const dataIndex = dataFromCurrentElement['dataIndex']
              const title = this.lineChartData['labels'][dataIndex];
              const datasets = this.lineChartData['datasets'][dataSetIndex]
              const label = this.lineChartData['datasets'][dataSetIndex]['data'][dataIndex];
              const currentElement = dataFromCurrentElement.dataIndex;
              const currentDataToShow = currentElement.label;
              const innerHtml = `
                        <div class="graph-tooltip">
                           
                                    <h6>${title} : ${label} ${datasets.label}</h6>
                                    <div>
                                    <ul>
                                     <li>kelly Blue Book: ${label} </li>
                                     <li>Henson Brand: ${label}</li>
                                     <li>Buy Your Car: ${label}</li>
                                    <li>CarGuru: ${label}</li>
                                    <li>AutoTrader: ${label}</li>
                                    <li>Other: ${label}</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                         
                    `;

              tooltipEl.querySelector('table').innerHTML = innerHtml;
            }

            const position = context.chart.canvas.getBoundingClientRect();

            // Display, position, and set styles for font
            tooltipEl.style.opacity = '1';
            tooltipEl.style.position = 'absolute';
            tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
            tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
            tooltipEl.style.padding = tooltipModel.padding + 'px ' + tooltipModel.padding + 'px';
            tooltipEl.style.pointerEvents = 'none';
          }
        },
      },
      exporting: {
        enabled: true,
        showTable: false,
        fileName: "line-chart",
        buttons: {
          contextButton: {
            menuItems: ["downloadCSV", "downloadSVG", "downloadPNG", "downloadPDF", "downloadJPEG", "downloadXLS"]
          }
        }
      }
    }

    const ctx = (<any>document.getElementById('myChart')).getContext('2d');
    this.lineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.lineChartLables,
        datasets: this.lineChartDataset
      },
      options: this.lineChartOptions
    });
  }


  /**
   * 
   * @param tooltip details for the tooltip
   * @returns return custom html for tooltip
   */
  customTooltips(tooltip) {
    // Tooltip Element
    var tooltipEl = <any>document.getElementById('chartjs-tooltip');
    if (!tooltipEl) {
      tooltipEl = document.createElement('div');
      tooltipEl.id = 'chartjs-tooltip';
      tooltipEl.innerHTML = "<table></table>"
      document.body.appendChild(tooltipEl);
    }
    // Hide if no tooltip
    if (tooltip.opacity === 0) {
      tooltipEl.style.opacity = 0;
      return;
    }
    // Set caret Position
    tooltipEl.classList.remove('above', 'below', 'no-transform');
    if (tooltip.yAlign) {
      tooltipEl.classList.add(tooltip.yAlign);
    } else {
      tooltipEl.classList.add('no-transform');
    }
    function getBody(bodyItem) {
      return bodyItem.lines;
    }
    // Set Text
    if (tooltip.body) {
      var titleLines = tooltip.title || [];
      var bodyLines = tooltip.body.map(getBody);
      //PUT CUSTOM HTML TOOLTIP CONTENT HERE (innerHTML)
      var innerHtml = '<thead>';
      titleLines.forEach(function (title) {
        innerHtml += '<tr><th>' + title + '</th></tr>';
      });
      innerHtml += '</thead><tbody>';
      bodyLines.forEach(function (body, i) {
        var colors = tooltip.labelColors[i];
        var style = 'background:' + colors.backgroundColor;
        style += '; border-color:' + colors.borderColor;
        style += '; border-width: 2px';
        var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
        innerHtml += '<tr><td>' + span + body + '</td></tr>';
      });
      innerHtml += '</tbody>';
      var tableRoot = tooltipEl.querySelector('table');
      tableRoot.innerHTML = innerHtml;
    }
    var position = this.lineChart.canvas.getBoundingClientRect();
    tooltipEl.style.opacity = 1;
    tooltipEl.style.left = position.left + tooltip.caretX + 'px';
    tooltipEl.style.top = position.top + tooltip.caretY + 'px';
    tooltipEl.style.fontFamily = tooltip._fontFamily;
    tooltipEl.style.fontSize = tooltip.fontSize;
    tooltipEl.style.fontStyle = tooltip._fontStyle;
    tooltipEl.style.padding = tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
  }

}
