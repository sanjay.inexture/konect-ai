import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { CloudData, CloudOptions, TagCloudComponent } from 'angular-tag-cloud-module';
@Component({
  selector: 'app-word-cloud',
  templateUrl: './word-cloud.component.html',
  styleUrls: ['./word-cloud.component.scss']
})
export class WordCloudComponent implements OnInit {
  @ViewChild(TagCloudComponent) tagCloudComponent: TagCloudComponent;
  wordCloudOptions: CloudOptions = {
    width: 0.9,
    height: 300,
    overflow: false,
    realignOnResize: true
  };
  @Input() wordCloudData: CloudData[] = [] 
 
  constructor() { }

  ngOnInit(): void {
    this.wordCloudData.map((x:any) => {
      x.weight = parseInt(x.weight)/10
      return x
    })
  }

}
