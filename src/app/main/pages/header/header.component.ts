import { UtilService } from './../../../core/services/util/util.service';
import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public todayDate: Date = new Date();
  constructor(private utilservice:UtilService) { }

  ngOnInit(): void {
    //get current time using interval
    this.utilservice.timers.subscribe(() => {
      this.todayDate = new Date();
    })
  }

}
