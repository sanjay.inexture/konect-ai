import { OverviewService } from './../../../core/services/overview/overview.service';
import { Component, OnInit, ViewChild, Input } from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle,
  ApexYAxis,
  ApexDataLabels,
  ApexStroke,
  ApexTooltip,
  ApexPlotOptions,
  ApexGrid,
  ApexTheme,
  ApexLegend,
  ApexResponsive,
  ApexNoData
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  title: ApexTitleSubtitle;
  yaxis: ApexYAxis;
  dataLabels: ApexDataLabels,
  stroke: ApexStroke;
  plotOptions: ApexPlotOptions,
  tooltip: ApexTooltip,
  colors: ApexGrid,
  theme: ApexTheme,
  events: ApexChart,
  legend: ApexLegend,
  grid: ApexGrid,
  responsive: ApexResponsive,
  noData: ApexNoData
};

@Component({
  selector: 'app-heatmap-calander',
  templateUrl: './heatmap-calander.component.html',
  styleUrls: ['./heatmap-calander.component.scss']
})
export class HeatmapCalanderComponent implements OnInit {
  @ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartSeriesData: any[] = []
  xAxisLabels: string[] = [];

  constructor(public overviewService: OverviewService) {
    let that = this
    this.chartOptions = {
      series: [...this.chartSeriesData.reverse()],
      chart: {
        type: "heatmap",
        toolbar: {
          show: false,
        },
        redrawOnWindowResize: true,
        height: '550px',
        width: '100%',
        events: {
          mounted: function () {
            that.addYAxisBackground()
          },
          updated: function () {
            that.addYAxisBackground()
          }
        }
      },
      dataLabels: {
        enabled: false,
      },
      yaxis: {
        labels: {
          maxWidth: 250,
          style: {
            fontSize: "8px",
            fontWeight: 400,
            fontFamily: "DM Sans",
            colors: ["#000000"],
          },
        },
      },
      stroke: {
        width: 0
      },
      xaxis: {
        categories: [...this.xAxisLabels],
        type: "category",
        position: 'top',
        labels: {
          style: {
            colors: [
              "#ffffff",
              "#ffffff",
              "#ffffff",
              "#ffffff",
              "#ffffff",
              "#ffffff",
              "#ffffff",
            ],
            fontWeight: "600",
          },
        },
        tooltip: {
          enabled: false,
        },
        axisTicks: {
          show: false,
        },
        axisBorder: {
          show: false,
        }
      },
      plotOptions: {
        heatmap: {
          shadeIntensity: 0.5,
          radius: 0,
          useFillColorAsStroke: true,
          colorScale: {
            ranges: [
              {
                from: 400,
                to: 10000,
                name: " ",
                color: "#FFFFFF"
              },
              {
                color: "#FFFFFF",
                name: "0",
              },
              {
                from: 0,
                to: 100,
                color: "#F6F8FA",
                name: "100",
              },
              {
                from: 101,
                to: 200,
                color: "#E8EBF0",
                name: "200",
              },
              {
                from: 201,
                to: 300,
                color: "#8FA5C9",
                name: "300",

              },
              {
                from: 301,
                to: 400,
                color: "#306DCC",
                name: "400",
              },
            ],
          }
        }
      },
      grid: {
        padding: {
          left: 20,
          right: 0
        }
      },
      tooltip: {
        marker: {
          show: true,
        },
        custom: function ({ series, seriesIndex, dataPointIndex, w }) {
          const tooltip = w.config.series[seriesIndex].tooltip[dataPointIndex];
          const tooltip_dates = w.config.series[seriesIndex].tooltip_dates[dataPointIndex];
          series = series.reverse();
          return (
            `<div class="graph-tooltip" > 
            <h6><b>July 13  ${w.globals.labels[dataPointIndex]} : ${series[seriesIndex][dataPointIndex]} Engagement</b></h6>
            <ul>
            <li>kelly Blue Book: ${series[seriesIndex][dataPointIndex]} </li>
            <li>Henson Brand: ${series[seriesIndex][dataPointIndex]}</li>
            <li>Buy Your Car: ${series[seriesIndex][dataPointIndex]}</li>
           <li>CarGuru: ${series[seriesIndex][dataPointIndex]}</li>
           <li>AutoTrader: ${series[seriesIndex][dataPointIndex]}</li>
           <li>Other: ${series[seriesIndex][dataPointIndex]}</li>
           </ul>
            </div>`
          );
        },
      },
      legend: {
        show: false,
        showForSingleSeries: true,
        position: 'bottom',
        height: 60,
        horizontalAlign: "right",
        markers: {
          width: 60,
          radius: 0,
          offsetX: 0,
          offsetY: 15,
        }
      },
      responsive:
      {
        breakpoint: 300,
      },
      noData: {
        text: 'No Data Available',
        align: "center",
        verticalAlign: "middle",
      },
    };
  }

  /**
   * add background color of y axis
   */
  addYAxisBackground() {
    var ctx = <any>document.querySelector("svg"),
      textElm = ctx.querySelector("svg g"),
      SVGRect = textElm.getBBox();
    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("x", SVGRect.x);
    rect.setAttribute("y", SVGRect.y + 63);
    rect.setAttribute("width", "58");
    rect.setAttribute("height", (SVGRect.height - 22).toString());
    rect.setAttribute("fill", "#E8EBF0");
    ctx.insertBefore(rect, textElm);
    this.addXAxisBackground()
  }

  /**
   * add background color of x axis
   */
  addXAxisBackground() {
    var ctx = <any>document.querySelector("svg"),
      textElm = ctx.querySelector("svg g"),
      SVGRect = textElm.getBBox();
    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("x", SVGRect.x + 57);
    rect.setAttribute("y", SVGRect.y + 32);
    rect.setAttribute("width", SVGRect.width);
    rect.setAttribute("height", "32px");
    rect.setAttribute("fill", "#17304E");
    ctx.insertBefore(rect, textElm);
    this.addBoxBackground()
  }

  /**
   * add background color of white box
   */
  addBoxBackground() {
    var ctx = <any>document.querySelector("svg"),
      textElm = ctx.querySelector("svg g"),
      SVGRect = textElm.getBBox();
    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute("y", SVGRect.y + 32);
    rect.setAttribute("width", "58");
    rect.setAttribute("height", "32px");
    rect.setAttribute("fill", "#F6F8FA");
    ctx.insertBefore(rect, textElm);
  }

  ngOnInit(): void {
    this.overviewService.heatMapSubscription.subscribe(response => {
      if (response) {
        this.xAxisLabels = response.categories;
        this.chartSeriesData = response.series;
        this.chartOptions = {
          ...this.chartOptions, ...{
            series: this.chartSeriesData,
            xaxis: {
              categories: [...this.xAxisLabels],
              position: 'top',
              labels: {
                style: {
                  colors: [
                    "#ffffff",
                    "#ffffff",
                    "#ffffff",
                    "#ffffff",
                    "#ffffff",
                    "#ffffff",
                    "#ffffff",
                  ],
                  fontWeight: "600",
                },
              },
              tooltip: {
                enabled: false,
              },
              axisTicks: {
                show: false,
              },
              axisBorder: {
                show: false,
              }
            }
          }
        }
      }
    })
  }
}
