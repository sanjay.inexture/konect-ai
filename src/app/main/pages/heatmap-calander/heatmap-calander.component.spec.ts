import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeatmapCalanderComponent } from './heatmap-calander.component';

describe('HeatmapCalanderComponent', () => {
  let component: HeatmapCalanderComponent;
  let fixture: ComponentFixture<HeatmapCalanderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeatmapCalanderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeatmapCalanderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
