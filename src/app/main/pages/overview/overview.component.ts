import { Subject, takeUntil } from 'rxjs';
import { OverviewService } from './../../../core/services/overview/overview.service';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UtilService } from 'src/app/core/services/util/util.service';
declare var $: any
import * as moment from 'moment';
import { TimePeriodEnum } from '../common-enum/CommonEnums';
import { IDashboardList, IDashboardObj, IInBoundList, IOutBoundList } from '../../../core/model/overview-model'

declare const Lightpick: any

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})

export class OverviewComponent implements OnInit, AfterViewInit {
  public todayDate: Date = new Date();
  public fromDate: moment.Moment = moment().clone().subtract(6, 'days');
  public toDate: moment.Moment = moment();
  public currentDateLabel: string = TimePeriodEnum.This_Week;
  public selectedDate: string;
  public subscriber: Subject<void>;
  public leadSourceData: string[] = [];
  public dashboardData: IDashboardList;
  public wordCloudData: any[]=[];
  public inboundData: IInBoundList[];
  public outboundData: IOutBoundList[];
  public selectedLeadSources: string[] = [];
  public dateDiff: number = 7;
  public dashboardObj: IDashboardObj = {
    days: this.dateDiff,
    type: "inbound",
    lead_source: this.selectedLeadSources
  }
  public selectDatePicker: any;
  public customDatePicker: any;
  constructor(public utilService: UtilService, public overviewService: OverviewService) { }

  ngOnInit(): void {
    this.subscriber = new Subject<void>();
    this.getDashboardData()
    this.getLeadSourceData()
    this.getBoundData('inbound')
    this.getBoundData('outbound')
    this.getHeapMapData()
    this.getWordCloudData()
  }

  ngAfterViewInit(): void {
    let that = this;
    var $boundEventSelect = $('.bound-dropdown').select2({
      minimumResultsForSearch: Infinity,
    });
    var $timePeriodEventSelect = $('.time-period-dropdown').select2({
      minimumResultsForSearch: Infinity,
    });

    $boundEventSelect.on("select2:select", function (e) {
      that.selectedBoundData(e)
    });

    $timePeriodEventSelect.on("select2:select", function (e) {
      that.openModal(e)
    });


    this.customDatePicker = new Lightpick({
      field: document.getElementById('from_date'),
      secondField: document.getElementById('to_date'),
      parentEl: '.modal-body',
      singleDate: false,
      numberOfMonths: 2,
      repick: false,
      hideOnBodyClick: false,
      autoClose: false,
      inline: true,
      maxDate: moment(),
      onSelect: function (start: any, end: any) {
        // var str = '';
        // str += start ? start.format('Do MMMM YYYY') + ' to ' : '';
        // str += end ? end.format('Do MMMM YYYY') : '...';
        // (<any>document.getElementById('result-3')).innerHTML = str;
      }
    });

    const selected_date = document.getElementById('selected_date')
    this.selectDatePicker = new Lightpick({
      field: selected_date,
      singleDate: false,
      selectForward: true,
      repick: true,
      startDate: moment().clone().subtract(6, 'days'),
      endDate: moment(),
      maxDate: moment(),
      minDate: moment(this.fromDate),
      inline: false,
      onSelect: function (start: any, end: any) {
        var str = '';
        start ? that.fromDate = start : that.fromDate.format('MMM D YYYY')
        end ? that.toDate = end : that.toDate.format('MMM D YYYY')
        str += start ? start.format('MMM D YYYY') + ' - ' : '';
        str += end ? end.format('MMM D YYYY') : '...';
        (<HTMLInputElement>selected_date).value = str;
        that.dateDiff = that.toDate.diff(that.fromDate, 'days');
        that.dashboardObj.days = that.dateDiff + 1;
        that.reloadData();
      }
    });
    var str = '';
    str += this.fromDate.format('MMM D YYYY') + ' - ';
    str += this.toDate.format('MMM D YYYY');
    (<HTMLInputElement>selected_date).value = str;
  }

  reloadData() {
    this.getDashboardData();
    this.getHeapMapData();
    this.getWordCloudData();
  }

  /**
   * get Dashboard data
   */
  getDashboardData() {
    this.overviewService.getDashboardData(this.dashboardObj).pipe(takeUntil(this.subscriber)).subscribe((data) => {
      if (data.status === 1) {
        let response = data.data;
        this.dashboardData = response;
      }
    })
  }

  /**
   * save selected custom date
   */
  saveCustomDate() {
    this.currentDateLabel = TimePeriodEnum.Custom;
    this.fromDate = this.customDatePicker.getStartDate()
    this.toDate = this.customDatePicker.getEndDate()
    this.selectDatePicker.setDateRange(this.fromDate, this.toDate);
    this.selectDatePicker.reloadOptions({
      minDate: this.fromDate,
      maxDate: moment()
    })
  }

  /**
   * selected bound
   * @param target current element
   */
  selectedBoundData(event: { target: HTMLInputElement }) {
    const value = event.target.value;
    value === 'Both' ? delete this.dashboardObj.type : this.dashboardObj.type = value;
    this.reloadData()
  }

  /**
   * get lead source data
   */
  getLeadSourceData() {
    let that = this;
    this.overviewService.getLeadSourceData().pipe(takeUntil(this.subscriber)).subscribe((data) => {
      if (data.status === 1) {
        let response = data.data;
        this.leadSourceData = response;

        $(document).ready(function () {
          //generate multiple select checkbox
          $('#multiple-checkboxes ,#multiple-checkboxes-mobile').multiselect({
            includeSelectAllOption: true,
          });

          $('.multi-select').on('change', () => {
            var brands = $('.multi-select option:selected');
            that.selectedLeadSources = [];
            $(brands).each(function (index, brand) {
              that.selectedLeadSources.push($(this).val());
            });
            that.dashboardObj.lead_source = that.selectedLeadSources
            that.reloadData()
          })
        });
      }
    })
  }

  /**
   * get inbound and outbound data
   * @param type inbound/outbound
   */
  getBoundData(type: string) {
    const dashboardObj = Object.assign({}, this.dashboardObj);
    dashboardObj.type = type;
    this.overviewService.getBoundData(dashboardObj).subscribe((data) => {
      if (data.status === 1) {
        let response = data.data;
        type === 'inbound' ? this.inboundData = response : this.outboundData = response;
      }
    })
  }

  /**
   * get Heat map data
   */
  getHeapMapData(){
    this.overviewService.getHeapMapData(this.dashboardObj).pipe(takeUntil(this.subscriber)).subscribe((data) => {
      if (data.status === 1) {
        let response = data.data;
        this.overviewService.heatMapSubject.next(response)
      }
    })
  }

  /**
 * get word cloud map data
 */
  getWordCloudData() {
    this.overviewService.getWordCloudData(this.dashboardObj).pipe(takeUntil(this.subscriber)).subscribe((data) => {
      if (data.status === 1) {
        let response = data.data;
        this.wordCloudData = response;;
      }
    })
  }

  /**
   * open print page popup dialog
   */
  public openPrintPopup(): void {
    window.print();
  }

  /**
   * open custom date selection modal
   * @param target selected options
   */
  openModal(event: { target: HTMLInputElement }) {
    this.selectedDate = event.target.value;
    event.target.value === 'Custom' ? $("#dateBackdrop").modal('show') : this.updateDatePicker(this.selectedDate);
  }

  /**
   * update date picker on change Time Period
   * @param value selected type of TimePeriodEnum
   */
  updateDatePicker(value: string): void {
    switch (value) {
      case TimePeriodEnum.This_Week:
        this.currentDateLabel = TimePeriodEnum.This_Week;
        this.fromDate = moment().clone().subtract(6, 'days');
        break;
      case TimePeriodEnum.This_Month:
        this.currentDateLabel = TimePeriodEnum.This_Month;
        this.fromDate = moment(moment().startOf('month').format('YYYY-MM-DD'));
        break;
      case TimePeriodEnum.This_Year:
        this.currentDateLabel = TimePeriodEnum.This_Year;
        this.fromDate = moment(moment().startOf('year').format('YYYY-MM-DD'));
        break;
    }

    this.toDate = moment();
    this.selectDatePicker.setDateRange(this.fromDate, this.toDate);
    this.selectDatePicker.reloadOptions({
      minDate: this.fromDate,
      maxDate: this.toDate
    })
  }

  /**
   * set bound td class using color value
   * @param value field value
   * @returns 
   */
  getBoundClass(value: number): string {
    return value >= 0 && value <= 100 ? 'white'
      : value >= 100 && value <= 200 ? 'white-blue'
        : value >= 200 && value <= 300 ? 'light-blue'
          : value >= 400 ? 'blue' : 'blue'
  }

  /**
   * set up and down arrow image and color of lable
   * @param value changes data value
   * @returns 
   */
  setImageAndColor(value: string, type: string): string {
    let img = '../../../../assets/static/images/up-arrow.png'
    let color = 'green'
    if (value === "NA") {
      img = '../../../../assets/static/images/side-arrow.png';
      color = 'blue'
    }
    if (parseFloat(value) / 100 >= 1) {
      img = '../../../../assets/static/images/up-arrow.png';
      color = 'green'
    }
    if (parseFloat(value) / 100 < 1) {
      img = '../../../../assets/static/images/down-orange-arrow.png';
      color = 'orange'
    }
    return type === 'image' ? img : color
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriber.next();
    this.subscriber.complete();
  }
}
