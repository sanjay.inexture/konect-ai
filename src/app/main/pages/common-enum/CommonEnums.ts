
export enum TimePeriodEnum {
 This_Week = "This Week",
 This_Month = "This Month",
 This_Year = "This Year",
 Custom = "Custom",
}